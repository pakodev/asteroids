using UnityEngine;
using Asteroids.Core;
using Asteroids.Config;
using Asteroids.Visual;
using Asteroids.UI;
using Asteroids.Input;

namespace Asteroids.Game {

    public class GameController : MonoBehaviour {
        readonly static string configPath = "Config/config";

        [SerializeField]
        AsteroidsUI ui;
        [SerializeField]
        SpaceObjectsVisualCreator visualCreator;

        SpaceContext _space;
        Systems _systems;

        AsteroidsConfig LoadConfig() {
            var testAsset = Resources.Load<TextAsset>(configPath);
            var config = JsonUtility.FromJson<AsteroidsConfig>(testAsset.text);
            Resources.UnloadAsset(testAsset);
            return config;
        }

        void Awake() {
            var config = LoadConfig();

            _space = new SpaceContext(config);

            ui.Init(_space);
            visualCreator.Init(_space);

            _systems = new Systems();
            _systems.Add(new InitializeSystem(_space));
            _systems.Add(new InputSystem(_space));
            _systems.Add(new EnemySpawnSystem(_space));
            _systems.Add(new EnemyMoveSystem(_space));
            _systems.Add(new ProjectileMoveSystem(_space));
            _systems.Add(new SpaceshipMoveSystem(_space));
            _systems.Add(new BoundsSystem(_space));
            _systems.Add(new ProjectileTtlSystem(_space));
            _systems.Add(new SpaceshipShotSystem(_space));
            _systems.Add(new ProjectileHitSystem(_space));
            _systems.Add(new SpaceshipExplosionSystem(_space));
            _systems.Add(new DestroySystem(_space));

            _systems.Initialize();
        }

        void Update() {
            _systems.Execute();
            _systems.Cleanup();
        }
    }

}
