using UnityEngine;
using Asteroids.Core;

namespace Asteroids.Visual {

    public class SpaceshipView : View {
#if UNITY_EDITOR
        float radius;

        public override void Link(SpaceObjectBase spaceObject) {
            base.Link(spaceObject);

            if (spaceObject is Spaceship spaceship) {
                radius = spaceship.Config.radius;
            }
        }

        private void OnDrawGizmos() {
            Color oldColor = Gizmos.color;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(viewTransform.position, radius);
            Gizmos.color = oldColor;
        }
#endif
    }

}