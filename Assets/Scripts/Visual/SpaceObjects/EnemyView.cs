using UnityEngine;
using Asteroids.Core;

namespace Asteroids.Visual {

    public class EnemyView : View {
#if UNITY_EDITOR
        float radius;

        public override void Link(SpaceObjectBase spaceObject) {
            base.Link(spaceObject);

            if (spaceObject is Enemy enemy) {
                radius = enemy.Config.radius;
            }
        }

        private void OnDrawGizmos() {
            Color oldColor = Gizmos.color;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(viewTransform.position, radius);
            Gizmos.color = oldColor;
        }
#endif
    }

}