using UnityEngine;
using Asteroids.Core;

namespace Asteroids.Visual {

    public class View : MonoBehaviour, IView, IMovable, IRotatable {
        [SerializeField]
        protected Transform viewTransform;

        public virtual void Link(SpaceObjectBase spaceObject) {
            spaceObject.MoveEvent += Move;
            spaceObject.RotateEvent += Rotate;
        }

        public virtual void Unlink(SpaceObjectBase spaceObject) {
            spaceObject.MoveEvent -= Move;
            spaceObject.RotateEvent -= Rotate;
        }

        public void Move(Vector2 position) {
            viewTransform.position = position;
        }

        public void Rotate(float angle) {
            viewTransform.localRotation = Quaternion.Euler(0, 0, angle);
        }
    }
}