using UnityEngine;
using Asteroids.Core;

namespace Asteroids.Visual {

    public class SpaceObjectsVisualCreator : MonoBehaviour {
        [SerializeField]
        Transform spaceTransform;
        [SerializeField]
        SpaceObjectsScriptableObject spaceObjects;

        SpaceContext _space = null;

        public void Init(SpaceContext space) {
            _space = space;

            _space.Factory.EnemyCreateEvent += OnEnemyCreate;
            _space.Factory.ProjectileCreateEvent += OnProjectileCreate;
            _space.Factory.SpaceshipCreateEvent += OnSpaceshipCreate;
        }

        private void OnEnemyCreate(Enemy enemy) {
            var obj = Instantiate(spaceObjects.GetEnemy(enemy.Config.prefab).gameObject, spaceTransform).GetComponent<View>();
            obj.Link(enemy);
            enemy.View = obj;
            enemy.DestroyEvent += OnDestroySpaceObject;
        }

        private void OnProjectileCreate(Projectile projectile) {
            var obj = Instantiate(spaceObjects.GetProjectile(projectile.Config.prefab).gameObject, spaceTransform).GetComponent<View>();
            obj.Link(projectile);
            projectile.View = obj;
            projectile.DestroyEvent += OnDestroySpaceObject;
        }

        private void OnSpaceshipCreate(Spaceship spaceship) {
            var obj = Instantiate(spaceObjects.spaceship.gameObject, spaceTransform).GetComponent<View>();
            obj.Link(spaceship);
            spaceship.View = obj;
            spaceship.DestroyEvent += OnDestroySpaceObject;
        }

        private void OnDestroySpaceObject(SpaceObjectBase spaceObject) {
            var view = (View)spaceObject.View;
            view.Unlink(spaceObject);
            Destroy(view.gameObject);
            spaceObject.View = null;
            spaceObject.DestroyEvent -= OnDestroySpaceObject;
        }
    }

}
