using UnityEngine;

namespace Asteroids.Visual {

    [CreateAssetMenu(fileName = "SpaceObjects", menuName = "Asteroids/SpaceObjects")]
    public class SpaceObjectsScriptableObject : ScriptableObject {
        public EnemyView[] enemies;
        public ProjectileView[] projectiles;
        public SpaceshipView spaceship;

        public EnemyView GetEnemy(string name) {
            for (int i = 0; i < enemies.Length; i++) {
                if (enemies[i].name == name) return enemies[i];
            }
            return null;
        }

        public ProjectileView GetProjectile(string name) {
            for (int i = 0; i < projectiles.Length; i++) {
                if (projectiles[i].name == name) return projectiles[i];
            }
            return null;
        }
    }

}
