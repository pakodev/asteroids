﻿namespace Asteroids.Core {

    public enum SpaceState {
        None    = 0,
        Start   = 1,
        Battle  = 2,
        End     = 3
    }

}
