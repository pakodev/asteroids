﻿namespace Asteroids.Core {
    
    public class DestroySystem : ICleanupSystem {
        private SpaceContext _space;

        public DestroySystem(SpaceContext space) {
            _space = space;
        }

        public void Cleanup() {
            var spaceObjects = _space.SpaceObjects;
            for (int i = spaceObjects.Count - 1; i >= 0; i--) {
                var spaceObject = spaceObjects[i];
                if (spaceObject.Destroyed) spaceObject.Destroy();
            }
        }
    }

}
