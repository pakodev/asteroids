﻿using UnityEngine;

namespace Asteroids.Core {

    public class InitializeSystem : IInitializeSystem {
        private SpaceContext _space;

        public InitializeSystem(SpaceContext space) {
            _space = space;
        }

        public void Initialize() {
            var camera = Camera.main;
            var height = camera.orthographicSize * 2.0f;
            var width = height * camera.aspect;

            _space.SpaceSize = new Vector2(width, height);

            _space.State = SpaceState.Start;
        }
    }

}