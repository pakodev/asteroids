﻿namespace Asteroids.Core {

    public interface IExecuteSystem: ISystem {
        void Execute();
    }

}
