﻿using UnityEngine;
using Asteroids.Config;

namespace Asteroids.Core {

    public class EnemyMoveSystem : IExecuteSystem {
        private SpaceContext _space;

        public EnemyMoveSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var enemies = _space.Enemies;

            for (int i = 0; i < enemies.Count; i++) {
                var enemy = enemies[i];
                var config = enemy.Config;

                if (config.speed > 0) {
                    var position = enemy.Position;

                    if (config.type == EnemyType.UFO) {
                        var spaceship = _space.Spaceship;
                        if (spaceship != null) {
                            var spdir = position - spaceship.Position;
                            enemy.Rotate(Mathf.Atan2(spdir.x, -spdir.y) * Mathf.Rad2Deg);
                        }
                    }

                    var radians = enemy.Angle * Mathf.Deg2Rad;
                    var direction = new Vector2(-Mathf.Sin(radians), Mathf.Cos(radians));
                    position += direction * Time.deltaTime * config.speed;
                    enemy.Move(position);
                }
            }
        }
    }

}
