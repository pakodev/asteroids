﻿using UnityEngine;

namespace Asteroids.Core {

    public class EnemySpawnSystem : IExecuteSystem {
        private SpaceContext _space;

        public EnemySpawnSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            if (_space.State != SpaceState.Battle) return;

            var condig = _space.Config.game;
            var enemySpawnDelay = _space.EnemySpawnDelay;

            enemySpawnDelay -= Time.deltaTime;

            while (enemySpawnDelay <= 0.0f && condig.enemySpawnDelay > 0) {
                enemySpawnDelay += condig.enemySpawnDelay;

                float totalWeight = 0.0f;
                for (int i = 0; i < condig.enemySpawnWeights.Count; i++) {
                    totalWeight += condig.enemySpawnWeights[i].weight;
                }

                float randomWeight = Random.Range(0.0f, totalWeight);
                float currentWeight = 0.0f;

                for (int i = 0; i < condig.enemySpawnWeights.Count; i++) {
                    currentWeight += condig.enemySpawnWeights[i].weight;
                    if (randomWeight <= currentWeight) {
                        var enemy = _space.Factory.CreateEnemy(_space.Config.GetEnemy(condig.enemySpawnWeights[i].enemyId));

                        var side = Random.Range(0, 4);
                        var halfSpaceSize = _space.SpaceSize / 2.0f;
                        switch (side) {
                            case 0:
                                enemy.Move(new Vector2(-halfSpaceSize.x, Random.Range(-halfSpaceSize.y, halfSpaceSize.y)));
                                break;
                            case 1:
                                enemy.Move(new Vector2(halfSpaceSize.x, Random.Range(-halfSpaceSize.y, halfSpaceSize.y)));
                                break;
                            case 2:
                                enemy.Move(new Vector2(Random.Range(-halfSpaceSize.x, halfSpaceSize.x), -halfSpaceSize.y));
                                break;
                            case 3:
                                enemy.Move(new Vector2(Random.Range(-halfSpaceSize.x, halfSpaceSize.x), halfSpaceSize.y));
                                break;
                        }

                        break;
                    }
                }
            }

            _space.EnemySpawnDelay = enemySpawnDelay;
        }
    }

}
