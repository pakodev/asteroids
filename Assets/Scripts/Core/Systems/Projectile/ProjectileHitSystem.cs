﻿using UnityEngine;

namespace Asteroids.Core {

    public class ProjectileHitSystem : IExecuteSystem {
        private SpaceContext _space;

        public ProjectileHitSystem(SpaceContext space) {
            _space = space;
        }

        float Dot(Vector2 p1, Vector2 p2) {
            return (p1.x * p2.x) + (p1.y * p2.y);
        }

        bool LineCircleIntersect(Vector2 p1, Vector2 p2, Vector2 cp, float radius) {
            var ac = cp - p1;
            var ab = p2 - p1;
            var ab2 = Dot(ab, ab);
            var acab = Dot(ac, ab);
            var t = acab / ab2;
            t = (t < 0) ? 0 : t;
            t = (t > 1) ? 1 : t;
            var h = new Vector2((ab.x * t + p1.x) - cp.x, (ab.y * t + p1.y) - cp.y);
            return Dot(h, h) <= radius * radius;
        }

        public void Execute() {
            var projectiles = _space.Projectiles;
            var enemies = _space.Enemies;

            for (int i = 0; i < projectiles.Count; i++) {
                var projectile = projectiles[i];
                var pconfig = projectile.Config;
                var radians = projectile.Angle * Mathf.Deg2Rad;
                var projectileDirection = new Vector2(-Mathf.Sin(radians), Mathf.Cos(radians));
                var projectilePosition = projectile.Position;

                for (int ei = 0; ei < enemies.Count; ei++) {
                    var enemy = enemies[ei];

                    if (enemy.Destroyed) continue;

                    var econfig = enemy.Config;

                    switch (pconfig.type) {
                        case Config.ProjectileType.Bullet:
                            if (Vector2.Distance(projectilePosition, enemy.Position) <= econfig.radius) {
                                projectile.MarkDestroy();
                                enemy.MarkDestroy();
                            }
                            break;
                        case Config.ProjectileType.Laser:
                            if (LineCircleIntersect(projectilePosition, projectilePosition + projectileDirection * 100.0f, enemy.Position, econfig.radius)) {
                                enemy.MarkDestroy();
                            }
                            break;
                    }

                    if (enemy.Destroyed) {
                        for (int si = 0; si < econfig.spawnEnemyIds.Count; si++) {
                            var spawnEnemy = _space.Factory.CreateEnemy(_space.Config.GetEnemy(econfig.spawnEnemyIds[si]));
                            spawnEnemy.Move(enemy.Position);
                        }
                        _space.Score += econfig.score;
                    }
                }
            }
        }
    }

}
