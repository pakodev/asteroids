﻿using UnityEngine;

namespace Asteroids.Core {

    public class ProjectileMoveSystem : IExecuteSystem {
        private SpaceContext _space;

        public ProjectileMoveSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var projectiles = _space.Projectiles;

            for (int i = 0; i < projectiles.Count; i++) {
                var projectile = projectiles[i];
                var config = projectile.Config;

                if (config.speed > 0) {
                    var position = projectile.Position;
                    var radians = projectile.Angle * Mathf.Deg2Rad;
                    var direction = new Vector2(-Mathf.Sin(radians), Mathf.Cos(radians));
                    position += direction * Time.deltaTime * config.speed;
                    projectile.Move(position);
                }
            }
        }
    }

}
