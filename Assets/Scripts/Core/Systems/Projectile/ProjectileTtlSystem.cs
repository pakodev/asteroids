﻿using UnityEngine;

namespace Asteroids.Core {

    public class ProjectileTtlSystem : IExecuteSystem {
        private SpaceContext _space;

        public ProjectileTtlSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var projectiles = _space.Projectiles;

            for (int i = 0; i < projectiles.Count; i++) {
                var projectile = projectiles[i];

                projectile.Ttl -= Time.deltaTime;

                if (projectile.Ttl <= 0) projectile.MarkDestroy();
            }
        }
    }

}
