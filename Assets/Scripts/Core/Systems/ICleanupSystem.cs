﻿namespace Asteroids.Core {

    public interface ICleanupSystem : ISystem {
        void Cleanup();
    }

}
