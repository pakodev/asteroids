﻿using UnityEngine;

namespace Asteroids.Core {

    public class SpaceshipMoveSystem : IExecuteSystem {
        private SpaceContext _space;

        public SpaceshipMoveSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var spaceship = _space.Spaceship;

            if (spaceship != null) {
                var config = spaceship.Config;

                var rotateDirection = (spaceship.IsLeft ? 1.0f : 0) + (spaceship.IsRight ? -1.0f : 0);

                if (rotateDirection != 0) {
                    var angle = spaceship.Angle + rotateDirection * Time.deltaTime * config.rotationSpeed;
                    if (angle < -180.0f) angle += 360.0f;
                    if (angle > 180.0f) angle -= 360.0f;
                    spaceship.Rotate(angle);
                }

                var moveVector = spaceship.MoveVector;

                if (spaceship.IsAceleration) {
                    var radians = spaceship.Angle * Mathf.Deg2Rad;
                    var direction = new Vector2(-Mathf.Sin(radians), Mathf.Cos(radians));
                    moveVector += direction * Time.deltaTime * config.engineAcceleration;
                    if (moveVector.magnitude > config.maxSpeed) {
                        moveVector = moveVector.normalized * config.maxSpeed;
                    }

                    spaceship.MoveVector = moveVector;
                }

                if (moveVector != Vector2.zero) {
                    var pos = spaceship.Position + moveVector * Time.deltaTime;
                    spaceship.Move(pos);
                }
            }
        }
    }

}
