﻿using UnityEngine;

namespace Asteroids.Core {

    public class SpaceshipShotSystem : IExecuteSystem {
        private SpaceContext _space;

        public SpaceshipShotSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var spaceship = _space.Spaceship;

            if (spaceship != null) {
                var config = spaceship.Config;

                var bulletShotDelay = spaceship.BulletShotDelay;
                bulletShotDelay -= Time.deltaTime;

                if (bulletShotDelay <= 0 && spaceship.IsBulletShot) {
                    bulletShotDelay += config.bulletShotDelay;

                    var bulletConfig = _space.Config.GetProjectile(config.bulletId);
                    _space.Factory.CreateProjectile(bulletConfig, spaceship);
                }

                spaceship.BulletShotDelay = bulletShotDelay < 0 ? 0 : bulletShotDelay;

                if (spaceship.LaserShots < config.laserMaxCount) {
                    var laserCooldown = spaceship.LaserCooldown;

                    laserCooldown -= Time.deltaTime;

                    while (laserCooldown <= 0.0f) {
                        spaceship.LaserShots++;
                        laserCooldown += config.laserCooldown;
                    }

                    spaceship.LaserCooldown = spaceship.LaserShots < config.laserMaxCount ? laserCooldown : 0;
                }

                if (spaceship.LaserShots > 0 && spaceship.IsLaserShot) {
                    spaceship.LaserShots--;
                    if (spaceship.LaserCooldown <= 0) spaceship.LaserCooldown = config.laserCooldown;

                    var laserConfig = _space.Config.GetProjectile(config.laserId);
                    _space.Factory.CreateProjectile(laserConfig, spaceship);
                }
            }
        }
    }

}
