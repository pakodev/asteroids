﻿using UnityEngine;

namespace Asteroids.Core {

    public class SpaceshipExplosionSystem : IExecuteSystem {
        private SpaceContext _space;

        public SpaceshipExplosionSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var spaceship = _space.Spaceship;

            if (spaceship != null) {
                var enemies = _space.Enemies;

                var sconfig = spaceship.Config;

                for (int ei = 0; ei < enemies.Count; ei++) {
                    var enemy = enemies[ei];

                    if (enemy.Destroyed) continue;

                    var econfig = enemy.Config;

                    if (Vector2.Distance(spaceship.Position, enemy.Position) < sconfig.radius + econfig.radius) {
                        _space.End();
                    }
                }
            }
        }
    }

}
