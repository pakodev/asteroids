﻿namespace Asteroids.Core {

    public interface IInitializeSystem : ISystem {
        void Initialize();
    }

}
