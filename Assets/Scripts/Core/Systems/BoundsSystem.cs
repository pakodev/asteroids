﻿namespace Asteroids.Core {

    public class BoundsSystem : IExecuteSystem {
        private SpaceContext _space;

        public BoundsSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var spaceObjects = _space.SpaceObjects;
            var spaceSize = _space.SpaceSize;

            for (int i = 0; i < spaceObjects.Count; i++) {
                var spaceObject = spaceObjects[i];

                var position = spaceObject.Position;

                if (position.x < -spaceSize.x / 2) position.x += spaceSize.x;
                else if (position.x > spaceSize.x / 2) position.x -= spaceSize.x;

                if (position.y < -spaceSize.y / 2) position.y += spaceSize.y;
                else if (position.y > spaceSize.y / 2) position.y -= spaceSize.y;

                if (position != spaceObject.Position) spaceObject.Move(position);
            }
        }
    }

}
