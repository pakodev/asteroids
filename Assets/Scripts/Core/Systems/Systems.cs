using System.Collections.Generic;

namespace Asteroids.Core {

    public class Systems : ICleanupSystem, IInitializeSystem, IExecuteSystem, ISystem {
        List<IExecuteSystem> _executeSystems = new List<IExecuteSystem>();
        List<IInitializeSystem> _initializeSystems = new List<IInitializeSystem>();
        List<ICleanupSystem> _cleanupSystems = new List<ICleanupSystem>();

        public void Execute() {
            for (int i = 0; i < _executeSystems.Count; i++) {
                _executeSystems[i].Execute();
            }
        }

        public void Initialize() {
            for (int i = 0; i < _initializeSystems.Count; i++) {
                _initializeSystems[i].Initialize();
            }
        }

        public void Cleanup() {
            for (int i = 0; i < _cleanupSystems.Count; i++) {
                _cleanupSystems[i].Cleanup();
            }
        }

        public void Add(ISystem system) {
            if (system is IExecuteSystem executeSystem)
                _executeSystems.Add(executeSystem);
            if (system is IInitializeSystem initializeSystem)
                _initializeSystems.Add(initializeSystem);
            if (system is ICleanupSystem cleanupSystem)
                _cleanupSystems.Add(cleanupSystem);
        }

        public void Remove(ISystem system) {
            if (system is IExecuteSystem executeSystem)
                _executeSystems.Remove(executeSystem);
            if (system is IInitializeSystem initializeSystem)
                _initializeSystems.Remove(initializeSystem);
            if (system is ICleanupSystem cleanupSystem)
                _cleanupSystems.Remove(cleanupSystem);
        }
    }

}
