using UnityEngine;
using Asteroids.Config;

namespace Asteroids.Core {

    public class SpaceFactory {
        public delegate void EnemyCreateDelegate(Enemy enemy);
        public event EnemyCreateDelegate EnemyCreateEvent;

        public delegate void ProjectileCreateDelegate(Projectile projectile);
        public event ProjectileCreateDelegate ProjectileCreateEvent;

        public delegate void SpaceshipCreateDelegate(Spaceship spaceship);
        public event SpaceshipCreateDelegate SpaceshipCreateEvent;

        public Enemy CreateEnemy(EnemyConfig config) {
            Enemy enemy = new Enemy(config);

            EnemyCreateEvent?.Invoke(enemy);

            enemy.Move(Vector2.zero);
            enemy.Rotate(Random.Range(0.0f, 360.0f));

            return enemy;
        }

        public Projectile CreateProjectile(ProjectileConfig config, SpaceObjectBase spaceObject) {
            Projectile projectile = new Projectile(config);

            ProjectileCreateEvent?.Invoke(projectile);

            projectile.Ttl = config.ttl;
            projectile.Move(spaceObject.Position);
            projectile.Rotate(spaceObject.Angle);

            return projectile;
        }

        public Spaceship CreateSpaceship(SpaceshipConfig config) {
            Spaceship spaceship = new Spaceship(config);

            SpaceshipCreateEvent?.Invoke(spaceship);

            spaceship.MoveVector = Vector2.zero;
            spaceship.LaserShots = config.laserMaxCount;
            spaceship.LaserCooldown = 0f;
            spaceship.Move(Vector2.zero);
            spaceship.Rotate(0.0f);

            return spaceship;
        }
    }

}
