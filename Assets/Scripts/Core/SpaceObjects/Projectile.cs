using Asteroids.Config;

namespace Asteroids.Core {

    public class Projectile : SpaceObjectBase {
        public ProjectileConfig Config { private set; get; }

        public float Ttl { set; get; }

        public Projectile(ProjectileConfig config) {
            Config = config;
        }
    }

}