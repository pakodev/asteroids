using Asteroids.Config;

namespace Asteroids.Core {

    public class Enemy : SpaceObjectBase {
        public EnemyConfig Config { private set; get; }

        public Enemy(EnemyConfig config) {
            Config = config;
        }
    }

}