using UnityEngine;
using Asteroids.Config;

namespace Asteroids.Core {

    public class Spaceship : SpaceObjectBase {
        public SpaceshipConfig Config { private set; get; }

        public bool IsAceleration { set; get; }
        public bool IsLeft { set; get; }
        public bool IsRight { set; get; }
        public bool IsBulletShot { set; get; }
        public bool IsLaserShot { set; get; }

        public float BulletShotDelay { set; get; }

        private Vector2 _moveVector = Vector2.zero;

        public delegate void MoveVectorDelegate(Vector2 spaceState);
        public event MoveVectorDelegate MoveVectorEvent;
        public Vector2 MoveVector {
            set {
                if (_moveVector == value) return;
                _moveVector = value;
                MoveVectorEvent?.Invoke(_moveVector);
            }
            get { return _moveVector; }
        }

        private int _laserShots = 0;

        public delegate void LaserShotsDelegate(int laserShots);
        public event LaserShotsDelegate LaserShotsEvent;
        public int LaserShots {
            set {
                if (_laserShots == value) return;
                _laserShots = value;
                LaserShotsEvent?.Invoke(_laserShots);
            }
            get { return _laserShots; }
        }

        private float _laserCooldown = 0;

        public delegate void LaserCooldownDelegate(float laserCooldown);
        public event LaserCooldownDelegate LaserCooldownEvent;
        public float LaserCooldown {
            set {
                if (_laserCooldown == value) return;
                _laserCooldown = value;
                LaserCooldownEvent?.Invoke(_laserCooldown);
            }
            get { return _laserCooldown; }
        }

        public Spaceship(SpaceshipConfig config) {
            Config = config;
        }
    }

}