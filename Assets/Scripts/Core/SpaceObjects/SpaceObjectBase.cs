using UnityEngine;

namespace Asteroids.Core {

    public abstract class SpaceObjectBase : IMovable, IRotatable, IDestroyable {
        public delegate void MoveDelegate(Vector2 position);
        public event MoveDelegate MoveEvent;

        public delegate void RotateDelegate(float angle);
        public event RotateDelegate RotateEvent;

        public delegate void DestroyDelegate(SpaceObjectBase spaceObject);
        public event DestroyDelegate DestroyEvent;

        public Vector2 Position { private set; get; }
        public float Angle { private set; get; }
        public bool Destroyed { private set; get; }
        public IView View { set; get; }

        public virtual void Move(Vector2 position) {
            Position = position;
            MoveEvent?.Invoke(position);
        }

        public virtual void Rotate(float angle) {
            Angle = angle;
            RotateEvent?.Invoke(angle);
        }

        public virtual void MarkDestroy() {
            Destroyed = true;
        }

        public virtual void Destroy() {
            DestroyEvent?.Invoke(this);
        }
    }

}