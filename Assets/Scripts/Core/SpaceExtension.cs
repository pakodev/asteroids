﻿namespace Asteroids.Core {

    public static class SpaceExtension {
        public static void Start(this SpaceContext space) {
            space.DestroyAll();

            var config = space.Config;

            space.Factory.CreateSpaceship(config.spaceship);

            space.State = SpaceState.Battle;
            space.Score = 0;
            space.EnemySpawnDelay = 0.0f;
        }

        public static void End(this SpaceContext space) {
            space.Spaceship.MarkDestroy();

            space.State = SpaceState.End;
        }
    }

}
