using UnityEngine;

namespace Asteroids.Core {

    public interface IMovable {
        void Move(Vector2 position);
    }

}