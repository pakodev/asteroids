namespace Asteroids.Core {

    public interface IDestroyable {
        void MarkDestroy();
        void Destroy();
    }

}