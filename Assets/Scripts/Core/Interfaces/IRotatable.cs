namespace Asteroids.Core {

    public interface IRotatable {
        void Rotate(float angle);
    }

}