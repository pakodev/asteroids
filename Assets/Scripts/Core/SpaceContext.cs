using UnityEngine;
using System.Collections.Generic;
using Asteroids.Config;

namespace Asteroids.Core {

    public class SpaceContext {
        public AsteroidsConfig Config { private set; get; }
        public SpaceFactory Factory { private set; get; }

        public List<SpaceObjectBase> SpaceObjects { private set; get; }
        public List<Enemy> Enemies { private set; get; }
        public List<Projectile> Projectiles { private set; get; }
        public Spaceship Spaceship { private set; get; }

        public Vector2 SpaceSize { set; get; }
        public float EnemySpawnDelay { set; get; }

        private SpaceState _spaceState = SpaceState.None;

        public delegate void SpaceStateDelegate(SpaceState spaceState);
        public event SpaceStateDelegate SpaceStateEvent;
        public SpaceState State {
            get { return _spaceState; }
            set {
                if (_spaceState == value) return;
                _spaceState = value;
                SpaceStateEvent?.Invoke(_spaceState);
            }
        }

        private int _score = 0;

        public delegate void ScoreDelegate(int score);
        public event ScoreDelegate ScoreEvent;
        public int Score {
            get { return _score; }
            set {
                if (_score == value) return;
                _score = value;
                ScoreEvent?.Invoke(_score);
            }
        }

        public SpaceContext(AsteroidsConfig config) {
            Config = config;

            SpaceObjects = new List<SpaceObjectBase>();
            Enemies = new List<Enemy>();
            Projectiles = new List<Projectile>();
            Spaceship = null;

            Factory = new SpaceFactory();
            Factory.EnemyCreateEvent += OnEnemyCreate;
            Factory.ProjectileCreateEvent += OnProjectileCreate;
            Factory.SpaceshipCreateEvent += OnSpaceshipCreate;
        }

        void OnEnemyCreate(Enemy enemy) {
            SpaceObjects.Add(enemy);
            Enemies.Add(enemy);
            enemy.DestroyEvent += OnEnemyDestroy;
        }

        void OnEnemyDestroy(IDestroyable destroyable) {
            if (destroyable is Enemy enemy) {
                enemy.DestroyEvent -= OnEnemyDestroy;
                Enemies.Remove(enemy);
                SpaceObjects.Remove(enemy);
            }
        }

        void OnProjectileCreate(Projectile projectile) {
            SpaceObjects.Add(projectile);
            Projectiles.Add(projectile);
            projectile.DestroyEvent += OnProjectileDestroy;
        }

        void OnProjectileDestroy(IDestroyable destroyable) {
            if (destroyable is Projectile projectile) {
                projectile.DestroyEvent -= OnProjectileDestroy;
                Projectiles.Remove(projectile);
                SpaceObjects.Remove(projectile);
            }
        }

        void OnSpaceshipCreate(Spaceship spaceship) {
            if (Spaceship == null) {
                SpaceObjects.Add(spaceship);
                Spaceship = spaceship;
                Spaceship.DestroyEvent += OnSpaceshipDestroy;
            }
            else throw new System.Exception("Spaceship already exist!");
        }

        void OnSpaceshipDestroy(IDestroyable destroyable) {
            if (destroyable is Spaceship spaceship && Spaceship == spaceship) {
                spaceship.DestroyEvent -= OnProjectileDestroy;
                Spaceship = null;
                SpaceObjects.Remove(spaceship);
            }
        }

        public void DestroyAll() {
            for (int i = SpaceObjects.Count - 1; i >= 0; i--) {
                SpaceObjects[i].Destroy();
            }
        }
    }

}
