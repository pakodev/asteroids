using UnityEngine;
using Asteroids.Core;

using UInput = UnityEngine.Input;

namespace Asteroids.Input {

    public class InputSystem : IExecuteSystem, ICleanupSystem {
        private SpaceContext _space;

        public InputSystem(SpaceContext space) {
            _space = space;
        }

        public void Execute() {
            var spaceship = _space.Spaceship;

            if (spaceship != null) {
                spaceship.IsAceleration = UInput.GetKey(KeyCode.W) || UInput.GetKey(KeyCode.UpArrow);
                spaceship.IsLeft = UInput.GetKey(KeyCode.A) || UInput.GetKey(KeyCode.LeftArrow);
                spaceship.IsRight = UInput.GetKey(KeyCode.D) || UInput.GetKey(KeyCode.RightArrow);
                spaceship.IsBulletShot = UInput.GetKey(KeyCode.Space);
                spaceship.IsLaserShot = UInput.GetKeyDown(KeyCode.LeftControl);
            }
        }

        public void Cleanup() {
            var spaceship = _space.Spaceship;

            if (spaceship != null) {
                spaceship.IsLaserShot = false;
            }
        }
    }

}
