using System;

namespace Asteroids.Config {

    [Serializable]
    public class SpaceshipConfig {
        public int bulletId;
        public int laserId;

        public float radius = 1.0f;

        public float maxSpeed = 5.0f;
        public float rotationSpeed = 200.0f;
        public float engineAcceleration = 3.0f;

        public float bulletShotDelay = 0.2f;

        public int laserMaxCount = 3;
        public float laserCooldown = 5.0f;
    }

}
