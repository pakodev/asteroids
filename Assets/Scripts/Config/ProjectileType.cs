﻿
namespace Asteroids.Config {

    public enum ProjectileType {
        Bullet  = 0,
        Laser   = 1
    }

}
