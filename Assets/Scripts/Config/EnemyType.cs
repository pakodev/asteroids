﻿
namespace Asteroids.Config {

    public enum EnemyType {
        Asteroid    = 0,
        UFO         = 1
    }

}
