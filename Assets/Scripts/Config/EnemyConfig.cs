using System;
using System.Collections.Generic;

namespace Asteroids.Config {

    [Serializable]
    public class EnemyConfig {
        public int id;
        public string name;
        public EnemyType type;
        public string prefab;
        public float radius = 1.0f;
        public float speed = 1.0f;
        public List<int> spawnEnemyIds;
        public int score = 1;
    }

}
