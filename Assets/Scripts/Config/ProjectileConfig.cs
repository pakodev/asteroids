using System;

namespace Asteroids.Config {

    [Serializable]
    public class ProjectileConfig {
        public int id;
        public string name;
        public ProjectileType type;
        public string prefab;
        public float speed = 1.0f;
        public float ttl = 0.1f;
    }

}
