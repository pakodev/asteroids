﻿using System;
using System.Collections.Generic;

namespace Asteroids.Config {

    [Serializable]
    public class EnemySpawnWeight {
        public int enemyId;
        public int weight;
    }

    [Serializable]
    public class GameConfig {
        public float enemySpawnDelay;
        public List<EnemySpawnWeight> enemySpawnWeights;
    }

}
