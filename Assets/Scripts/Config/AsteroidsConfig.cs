using System;
using System.Collections.Generic;

namespace Asteroids.Config {

    [Serializable]
    public class AsteroidsConfig {
        public GameConfig game;
        public List<EnemyConfig> enemies;
        public List<ProjectileConfig> projectiles;
        public SpaceshipConfig spaceship;

        public EnemyConfig GetEnemy(int id) {
            for (int i = 0; i < enemies.Count; i++) {
                if (enemies[i].id == id) return enemies[i];
            }
            return null;
        }

        public ProjectileConfig GetProjectile(int id) {
            for (int i = 0; i < projectiles.Count; i++) {
                if (projectiles[i].id == id) return projectiles[i];
            }
            return null;
        }
    }

}
