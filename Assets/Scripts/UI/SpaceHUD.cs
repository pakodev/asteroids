using UnityEngine;
using TMPro;
using Asteroids.Core;

namespace Asteroids.UI {

    public class SpaceHUD : MonoBehaviour {
        [SerializeField]
        TextMeshProUGUI scoreText;
        [SerializeField]
        TextMeshProUGUI xText;
        [SerializeField]
        TextMeshProUGUI yText;
        [SerializeField]
        TextMeshProUGUI angleText;
        [SerializeField]
        TextMeshProUGUI speedText;
        [SerializeField]
        TextMeshProUGUI laserShotsText;
        [SerializeField]
        TextMeshProUGUI laserCooldownText;

        SpaceContext _space;

        public void Init(SpaceContext space) {
            _space = space;

            _space.ScoreEvent += OnScore;
            _space.Factory.SpaceshipCreateEvent += OnSpaceshipCreate;

            Hide();
        }

        private void OnDestroy() {
            _space.ScoreEvent -= OnScore;
            _space.Factory.SpaceshipCreateEvent -= OnSpaceshipCreate;
        }

        public void Show() {
            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        void OnScore(int score) {
            scoreText.text = $"Score: {score}";
        }

        void OnMove(Vector2 position) {
            xText.text = $"X: {position.x:0.00}";
            yText.text = $"Y: {position.y:0.00}";
        }

        void OnRotate(float angle) {
            angleText.text = $"Angle: {angle:0.00}";
        }

        void OnMoveVector(Vector2 moveVector) {
            speedText.text = $"Speed: {moveVector.magnitude:0.00}";
        }

        void OnLaserShots(int laserShots) {
            laserShotsText.text = $"Laser shots: {laserShots}";
        }

        void OnLaserCooldown(float laserCooldown) {
            laserCooldownText.text = $"Laser cooldown: {laserCooldown:0.00}";
        }

        void OnSpaceshipCreate(Spaceship spaceship) {
            spaceship.MoveEvent += OnMove;
            spaceship.RotateEvent += OnRotate;
            spaceship.MoveVectorEvent += OnMoveVector;
            spaceship.LaserShotsEvent += OnLaserShots;
            spaceship.LaserCooldownEvent += OnLaserCooldown;
            spaceship.DestroyEvent += OnSpaceshipDestroy;
        }

        void OnSpaceshipDestroy(SpaceObjectBase spaceObject) {
            if (spaceObject is Spaceship spaceship) {
                spaceship.MoveEvent -= OnMove;
                spaceship.RotateEvent -= OnRotate;
                spaceship.MoveVectorEvent -= OnMoveVector;
                spaceship.LaserShotsEvent -= OnLaserShots;
                spaceship.LaserCooldownEvent -= OnLaserCooldown;
                spaceship.DestroyEvent -= OnSpaceshipDestroy;
            }
        }
    }

}