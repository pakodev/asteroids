using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Asteroids.Core;

namespace Asteroids.UI {

    public class EndUI : MonoBehaviour {
        [SerializeField]
        Button restartButton;
        [SerializeField]
        TextMeshProUGUI scoreText;

        SpaceContext _space;

        private void Awake() {
            restartButton.onClick.AddListener(OnRestart);
        }

        public void Init(SpaceContext space) {
            _space = space;

            Hide();
        }

        public void Show() {
            scoreText.text = $"Score: {_space.Score}";

            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        void OnRestart() {
            _space.Start();
        }
    }

}