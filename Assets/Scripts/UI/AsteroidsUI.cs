using UnityEngine;
using Asteroids.Core;

namespace Asteroids.UI {

    public class AsteroidsUI : MonoBehaviour {
        [SerializeField]
        StartUI startUI;
        [SerializeField]
        SpaceHUD spaceHUD;
        [SerializeField]
        EndUI endUI;

        SpaceContext _space;

        public void Init(SpaceContext space) {
            _space = space;

            _space.SpaceStateEvent += OnSpaceStateEvent;

            startUI.Init(_space);
            spaceHUD.Init(_space);
            endUI.Init(_space);
        }

        void OnSpaceStateEvent(SpaceState state) {
            switch (state) {
                case SpaceState.Start:
                    startUI.Show();
                    break;
                case SpaceState.Battle:
                    startUI.Hide();
                    endUI.Hide();
                    spaceHUD.Show();
                    break;
                case SpaceState.End:
                    spaceHUD.Hide();
                    endUI.Show();
                    break;
            }
        }

        private void OnDestroy() {
            _space.SpaceStateEvent -= OnSpaceStateEvent;
        }
    }

}
