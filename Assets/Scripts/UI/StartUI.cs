using UnityEngine;
using UnityEngine.UI;
using Asteroids.Core;

namespace Asteroids.UI {

    public class StartUI : MonoBehaviour {
        [SerializeField]
        Button startButton;

        SpaceContext _space;

        private void Awake() {
            startButton.onClick.AddListener(OnStart);
        }

        public void Init(SpaceContext space) {
            _space = space;

            Hide();
        }

        public void Show() {
            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        void OnStart() {
            _space.Start();
        }
    }

}
