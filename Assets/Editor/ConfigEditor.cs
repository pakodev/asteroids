using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using Asteroids.Config;
using Asteroids.Visual;
using System.Collections.Generic;
using System.Linq;

namespace Asteroids.Editor {

    public class ConfigEditor : EditorWindow {

        [MenuItem("Tools/Config Editor")]
        static void Init() {
            ConfigEditor window = new ConfigEditor();
            window.titleContent = new GUIContent("Config Editor");
            window.Show();
        }

        readonly string configPath = "Assets/Resources/Config/config.json";
        readonly string[] menuItems = {
            "Game",
            "Enemies",
            "Projectiles",
            "Spaceship"
        };

        ReorderableList _enemiesList = null;
        ReorderableList _projectilesList = null;
        ReorderableList _enemySpawnWieghtList = null;
        ReorderableList _enemySpawnList = null;

        int _currentMenuItems = 0;

        SpaceObjectsScriptableObject _prefabs = null;
        AsteroidsConfig _config = null;

        void OnGUI() {
            if (_prefabs == null) LoadPrefabs();
            if (_config == null) Load();

            EditorGUILayout.BeginVertical();

            EditorGUILayout.Separator();

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

            if (GUILayout.Button("Load", GUILayout.Width(100))) {
                Load();
            }

            if (GUILayout.Button("Save", GUILayout.Width(100))) {
                Save();
            }

            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
            _currentMenuItems = GUILayout.SelectionGrid(_currentMenuItems, menuItems, 4);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();

            EditorGUIUtility.labelWidth = 70;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Separator();

            if (_currentMenuItems == 0) OnGameGUI();
            else if (_currentMenuItems == 1) OnEnemiesGUI();
            else if (_currentMenuItems == 2) OnProjectilesGUI();
            else if (_currentMenuItems == 3) OnSpaceshipGUI();

            EditorGUILayout.Separator();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Separator();
            EditorGUILayout.EndVertical();
        }

        string EnemyPrefab(string prefab) {
            var names = new string[_prefabs.enemies.Length];
            var index = -1;
            for (int i = 0; i < _prefabs.enemies.Length; i++) {
                names[i] = _prefabs.enemies[i].name;
                if (names[i] == prefab) index = i;
            }
            index = EditorGUILayout.Popup("Prefab:", index, names);
            return index == -1 ? null : names[index];
        }

        string ProjectilePrefab(string prefab) {
            var names = new string[_prefabs.projectiles.Length];
            var index = -1;
            for (int i = 0; i < _prefabs.projectiles.Length; i++) {
                names[i] = _prefabs.projectiles[i].name;
                if (names[i] == prefab) index = i;
            }
            index = EditorGUILayout.Popup("Prefab:", index, names);
            return index == -1 ? null : names[index];
        }

        int ProjectileField(string title, int id) {
            var names = _config.projectiles.Select(p => p.name).ToArray();
            var ids = _config.projectiles.Select(p => p.id).ToArray();
            return EditorGUILayout.IntPopup(title, id, names, ids);
        }

        int EnemyField(Rect rect, int id) {
            var names = _config.enemies.Select(p => p.name).ToArray();
            var ids = _config.enemies.Select(p => p.id).ToArray();
            return EditorGUI.IntPopup(rect, id, names, ids);
        }

        void OnGameGUI() {
            var game = _config.game;

            if (game.enemySpawnWeights == null) game.enemySpawnWeights = new List<EnemySpawnWeight>();
            if (_enemySpawnWieghtList == null) {
                _enemySpawnWieghtList = new ReorderableList(game.enemySpawnWeights, typeof(EnemySpawnWeight));
                _enemySpawnWieghtList.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "Enemy spawn weights");
                _enemySpawnWieghtList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                    var spawnWeight = game.enemySpawnWeights[index];
                    spawnWeight.enemyId = EnemyField(new Rect(rect.x, rect.y, rect.width / 2f - 2f, rect.height), spawnWeight.enemyId);
                    EditorGUIUtility.labelWidth = 20;
                    spawnWeight.weight = EditorGUI.IntField(new Rect(rect.x + rect.width / 2f + 2f, rect.y, rect.width / 2f - 2f, rect.height - 4), "W:", spawnWeight.weight);
                };
                _enemySpawnWieghtList.onAddCallback = (list) => {
                    var spawnWeight = new EnemySpawnWeight();
                    game.enemySpawnWeights.Add(spawnWeight);
                };
            }

            EditorGUIUtility.labelWidth = 120;

            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(300));

            game.enemySpawnDelay = EditorGUILayout.FloatField("Enemy spawn delay:", game.enemySpawnDelay);

            EditorGUILayout.Separator();

            _enemySpawnWieghtList.DoLayoutList();

            EditorGUILayout.EndVertical();

            GUILayout.FlexibleSpace();
        }

        void OnEnemiesGUI() {
            if (_config.enemies == null) _config.enemies = new List<EnemyConfig>();
            if (_enemiesList == null) {
                _enemiesList = new ReorderableList(_config.enemies, typeof(EnemyConfig));
                _enemiesList.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "Enemies");
                _enemiesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                    var enemy = _config.enemies[index];
                    EditorGUI.LabelField(rect, $"ID: {enemy.id} Name: {enemy.name}");
                };
                _enemiesList.onAddCallback = (list) => {
                    var enemy = new EnemyConfig();
                    enemy.name = "New enemy";
                    enemy.id = _config.enemies.Count > 0 ? _config.enemies.Max((o) => o.id) + 1 : 1;
                    _config.enemies.Add(enemy);
                    list.Select(_config.enemies.Count - 1);
                };
                _enemiesList.onSelectCallback = (l) => {
                    _enemySpawnList = null;
                };
            }

            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(250));
            _enemiesList.DoLayoutList();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            if (_enemiesList.index >= 0 && _config.enemies.Count > 0) {
                var enemy = _config.enemies[_enemiesList.index];

                if (enemy.spawnEnemyIds == null) enemy.spawnEnemyIds = new List<int>();
                if (_enemySpawnList == null) {
                    _enemySpawnList = new ReorderableList(enemy.spawnEnemyIds, typeof(int));
                    _enemySpawnList.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "Spawn enemy");
                    _enemySpawnList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                        enemy.spawnEnemyIds[index] = EnemyField(rect, enemy.spawnEnemyIds[index]);
                    };
                }

                EditorGUILayout.Separator();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(250));

                EditorGUILayout.LabelField("ID: " + enemy.id);
                enemy.name = EditorGUILayout.TextField("Name:", enemy.name);

                EditorGUILayout.Separator();

                enemy.type = (EnemyType)EditorGUILayout.EnumPopup("Type:", enemy.type);
                enemy.prefab = EnemyPrefab(enemy.prefab);

                EditorGUILayout.Separator();

                enemy.speed = EditorGUILayout.FloatField("Speed:", enemy.speed);
                enemy.radius = EditorGUILayout.FloatField("Radius:", enemy.radius);

                EditorGUILayout.Separator();

                enemy.score = EditorGUILayout.IntField("Score:", enemy.score);

                EditorGUILayout.Separator();

                _enemySpawnList.DoLayoutList();

                EditorGUILayout.EndVertical();
            }

            GUILayout.FlexibleSpace();
        }

        void OnProjectilesGUI() {
            if (_config.projectiles == null) _config.projectiles = new List<ProjectileConfig>();
            if (_projectilesList == null) {
                _projectilesList = new ReorderableList(_config.projectiles, typeof(ProjectileConfig));
                _projectilesList.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "Projectiles");
                _projectilesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                    var projectile = _config.projectiles[index];
                    EditorGUI.LabelField(rect, $"ID: {projectile.id} Name: {projectile.name}");
                };
                _projectilesList.onAddCallback = (list) => {
                    var projectile = new ProjectileConfig();
                    projectile.name = "New projectile";
                    projectile.id = _config.projectiles.Count > 0 ? _config.projectiles.Max((o) => o.id) + 1 : 1;
                    _config.projectiles.Add(projectile);
                    list.Select(_config.projectiles.Count - 1);
                };
            }

            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(250));
            _projectilesList.DoLayoutList();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            if (_projectilesList.index >= 0 && _config.projectiles.Count > 0) {
                var projectile = _config.projectiles[_projectilesList.index];

                EditorGUILayout.Separator();

                EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(250));

                EditorGUILayout.LabelField("ID: " + projectile.id);
                projectile.name = EditorGUILayout.TextField("Name:", projectile.name);

                EditorGUILayout.Separator();

                projectile.type = (ProjectileType)EditorGUILayout.EnumPopup("Type:", projectile.type);
                projectile.prefab = ProjectilePrefab(projectile.prefab);

                EditorGUILayout.Separator();

                projectile.speed = EditorGUILayout.FloatField("Speed:", projectile.speed);
                projectile.ttl = EditorGUILayout.FloatField("TTL:", projectile.ttl);

                EditorGUILayout.EndVertical();
            }

            GUILayout.FlexibleSpace();
        }

        void OnSpaceshipGUI() {
            var spaceship = _config.spaceship;

            EditorGUIUtility.labelWidth = 120;

            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Width(250));

            spaceship.bulletId = ProjectileField("Bullet:", spaceship.bulletId);
            spaceship.laserId = ProjectileField("Laser:", spaceship.laserId);

            EditorGUILayout.Separator();

            spaceship.radius = EditorGUILayout.FloatField("Radius:", spaceship.radius);

            EditorGUILayout.Separator();

            spaceship.rotationSpeed = EditorGUILayout.FloatField("Rotation speed:", spaceship.rotationSpeed);
            spaceship.engineAcceleration = EditorGUILayout.FloatField("Aceleration:", spaceship.engineAcceleration);
            spaceship.maxSpeed = EditorGUILayout.FloatField("Max speed:", spaceship.maxSpeed);

            EditorGUILayout.Separator();

            spaceship.bulletShotDelay = EditorGUILayout.FloatField("Bullet shot delay:", spaceship.bulletShotDelay);

            EditorGUILayout.Separator();

            spaceship.laserMaxCount = EditorGUILayout.IntField("Laser max count:", spaceship.laserMaxCount);
            spaceship.laserCooldown = EditorGUILayout.FloatField("Laser cooldown:", spaceship.laserCooldown);

            EditorGUILayout.EndVertical();

            GUILayout.FlexibleSpace();
        }

        void LoadPrefabs() {
            _prefabs = AssetDatabase.LoadAssetAtPath<SpaceObjectsScriptableObject>("Assets/Sources/Prefabs/SpaceObjects.asset");
        }

        void Load() {
            if (File.Exists(configPath)) {
                var json = File.ReadAllText(configPath);
                _config = JsonUtility.FromJson<AsteroidsConfig>(json);
            }
            else _config = new AsteroidsConfig();

            _enemiesList = null;
            _projectilesList = null;
            _enemySpawnWieghtList = null;
        }

        void Save() {
            if (_config != null) {
                var json = JsonUtility.ToJson(_config, true);
                File.WriteAllText(configPath, json);
                AssetDatabase.ImportAsset(configPath, ImportAssetOptions.ForceUpdate);
            }
        }
    }

}
